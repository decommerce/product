# Product

## Initiate the Product project

### 0. Spring CLI

Initiate the project using spring cli with these dependencies.

- **data-mongodb**: Store data in flexible, JSON-like documents, meaning fields
- **flapdoodle-mongo**: Provides a platform neutral way for running MongoDB in unit
- **data-rest**: Exposing Spring Data repositories over REST via Spring Data
- **actuator**: Supports built in (or custom) endpoints that let you monitor

```sh
spring init \
    --build gradle \
    --java-version 17 \
    --dependencies data-mongodb,flapdoodle-mongo,data-rest,actuator \
    --groupId tk.decommerce \
    --name product \
    --package-name tk.decommerce.product \
    --force ./
```

### 1. Prepare Dev Environment

- Set environment variable in a Unix environment

```sh
export spring_profiles_active=dev
```

- Create a docker network named ***decommerce***

```sh
docker network create decommerce
```

- Run **MongoDB** in docker

```sh
docker run \
    -it --rm \
    --name mongo \
    --network decommerce \
    -p 27017:27017 \
    -e MONGO_INITDB_ROOT_USERNAME=root \
    -e MONGO_INITDB_ROOT_PASSWORD=root \
    mongo
```

- Run **MongoDB Express** in docker

```sh
docker run \
    -it --rm \
    --name mongo-express \
    --network decommerce \
    -p 8081:8081 \
    -e ME_CONFIG_MONGODB_ADMINUSERNAME="root" \
    -e ME_CONFIG_MONGODB_ADMINPASSWORD="root" \
    -e ME_CONFIG_MONGODB_SERVER="mongo" \
    mongo-express
```

- Run **Zookeeper** and **Kafka** in docker

```sh
docker run \
    -it --rm \
    --name zookeeper \
    --network decommerce \
    zookeeper
    
docker run \
    -it --rm \
    --name kafka \
    --network decommerce \
    -p 9092:9092 \
    -e KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper:2181 \
    -e ALLOW_PLAINTEXT_LISTENER=yes \
    bitnami/kafka
```

### 2. Add ***Mongock*** Dependency

Add this two dependencies into the `build.gradle`

```groovy
dependencies {
    ...
    implementation "com.github.cloudyrock.mongock:mongock-spring-v5:4.+"
    implementation "com.github.cloudyrock.mongock:mongodb-springdata-v3-driver:4.+"
    ...
}
```

### 3. Add ***Jib*** Plugin Dependency

```groovy
plugins {
    ...
    id 'com.google.cloud.tools.jib' version '3.1.4'
    ...
}
```