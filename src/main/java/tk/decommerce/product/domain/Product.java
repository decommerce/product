package tk.decommerce.product.domain;

import org.springframework.data.annotation.Id;

public record Product(@Id String id,
                      String name,
                      String code,
                      String title,
                      String description,
                      String imgUrl,
                      Double price,
                      String productCategoryName) {
}
