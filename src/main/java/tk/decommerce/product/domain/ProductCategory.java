package tk.decommerce.product.domain;

import org.springframework.data.annotation.Id;

public record ProductCategory(@Id String id,
                              String name,
                              String title,
                              String description,
                              String imgUrl) {
}
