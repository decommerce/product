package tk.decommerce.product.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tk.decommerce.product.domain.ProductCategory;

@RepositoryRestResource
public interface ProductCategoryRepository extends MongoRepository<ProductCategory, String> {
}
