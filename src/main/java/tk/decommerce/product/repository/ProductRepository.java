package tk.decommerce.product.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tk.decommerce.product.domain.Product;

import java.util.List;

@RepositoryRestResource
public interface ProductRepository extends MongoRepository<Product, String> {
    public List<Product> findByProductCategoryName(@Param("productCategoryName") String productCategoryName);
}
