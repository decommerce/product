package tk.decommerce.product.config;

import com.github.cloudyrock.spring.v5.EnableMongock;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories("tk.decommerce.product.repository")
@EnableMongock
public class DatabaseConfiguration {
}
