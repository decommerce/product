package tk.decommerce.product.config.dbmigrations;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import tk.decommerce.product.domain.Product;
import tk.decommerce.product.domain.ProductCategory;
import tk.decommerce.product.repository.ProductCategoryRepository;
import tk.decommerce.product.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

@ChangeLog(order = "001")
public class InitialSetupMigration {

    @ChangeSet(order = "001", id = "seedDataBase", author = "Hasan")
    public void seedDataBase(ProductRepository productRepository, ProductCategoryRepository productCategoryRepository) {
        List<ProductCategory> productCategories = new ArrayList<ProductCategory>();
        productCategories.add(new ProductCategory(null, "Stationery", "Stationery", "commercially manufactured writing materials", "https://pencils.co.tz/assets/images/stat_main.png"));
        productCategoryRepository.insert(productCategories);

        List<Product> products = new ArrayList<Product>();
        products.add(new Product(null, "Galileo Pen", "1578", "Pen", "Best pen ever", "", 12500.5, "Stationery"));
        products.add(new Product(null, "Neon Pen", "1579", "Pen", null, "", 15000.5, "Stationery"));
        products.add(new Product(null, "Alv Pen", "1580", "Pen", null, "", 9000.1, "Stationery"));
        products.add(new Product(null, "Vik Pen", "1581", "Pen", null, "", 60000.0, "Stationery"));
        products.add(new Product(null, "Trello Pen", "1582", "Pen", "Biggest in the store", "", 98500.0, "Stationery"));
        productRepository.insert(products);
    }
}
